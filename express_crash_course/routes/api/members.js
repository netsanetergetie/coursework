const express = require("express");
const router = express.Router()
const members = require('../../Members');
const uuid = require('uuid')

//router.get('/api/members', (req, res) => res.json(members))

// get all json objects 
router.get('/',(req,res)=> res.json(members));

// get single object
router.get('/:id', (req,res) => {
    const found = members.some(member => member.id === parseInt(req.params.id));
    
    if(found){
        res.json(members.filter(member => member.id === parseInt(req.params.id)));
    }else {
        res.status(400).json({msg:`No member with the id of ${req.params.id} found`});
    }

//res.send(req.query.id)
});
// Create Memeber/ adding to DB using POST request
router.post('/', (req, res) => {
    //res.send(req.body)
    const newMember = {
        id: uuid.v4(),
        name :req.body.name,
        email: req.body.email,
        status: 'active'
    }
    if (!newMember.name||!newMember.email)
{
return res.status(400).json({msg:' please include a name and email'})


}
members.push(newMember)
//res.json(members)

res.redirect('/')
});
// delet member
router.delete('/:id', (req,res) => {
    const found = members.some(member => member.id === parseInt(req.params.id));
    
    if(found){
 res.json({
            msg:'member deleted',
            members:members.filter(member => 
    member.id !== parseInt(req.params.id))});
    }else {
        res.status(400).json({msg:`No member with the id of ${req.params.id} found`});
    }
//res.send(req.query.id)
});


module.exports = router;