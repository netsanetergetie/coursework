const express = require('express');
const path = require('path');
const exphbs  = require('express-handlebars');

const logger =require('./middleware/logger')
//const moment = require('moment')
const members= require('./Members')
const app = express();


//const logger = require('./middleware/logger');
/*
app.get('/', (req,res)=>{
    res.sendFile(path.join(__dirname, 'public','index.html'))
    //res.send('<h1> hello world!! </h1>');
});*/

// Init middleware
//app.use(logger)

// handlebars middleware
// app.engine('handlebars', exphbs({defaultLayout: 'main'}));
// app.set('view engine', 'handlebars');

// Body Parser Middleware
app.use(express.json())
app.use(express.urlencoded({extended: false}))

//homepage route 
app.get('/', (req,res)=> res.render('index',
{'title': 'member app Netsi',
members:members }))

// app.engine('handlebars', exphbs({extname: '.hbs'}));
// app.set('view engine', '.hbs');
//app.engine('handlebars', exphbs.engine({defaultLayout:'main'}));
//app.engine('handlebars', exphbs({defaultLayout:'main'}))

app.engine('handlebars', exphbs.engine({defaultLayout: "main"}));
app.set('view engine', 'handlebars');



// Set static folder
app.use(express.static(path.join(__dirname,'public')));

//Memebers API Routes
app.use('/api/members/', require('./routes/api/members'));




const PORT = process.env.PORT || 8100
app.listen(PORT,()=> console.log(`Server started on port!! ${PORT}`))
 