
/*console.log('Netsanet is joining the class')
const Person = require("./person")

const p1= new Person('netsi',21)
p1.greething();
*/

const http = require('http')
const fs = require('fs')
const path =require('path')


const server = http.createServer((req,res) => {

    /*if (req.url=='/about')
    {
        fs.readFile(path.join(__dirname,'public','about.html'),
        (err,content)=> {
        if (err) throw err
        res.writeHead(200,{'content-type':'text/html'})
        res.end(content)
        })
  }
  if (req.url=='/api/users')
  {
        const users = [ { name :'netsi'}, 
                        {age:'21'}]
        res.writeHead(200,{'content-type':'application/json'})

        res.end(JSON.stringify(users))
  }
*/
    //find the path
    let filepath  = path.join(__dirname,
                            'public',
                        req.url==='/' ? 'index.html' : req.url 
                        );
    // get extenstion of file
    let extenname=path.extname(filepath)    
    // get content type 

    let contentType = 'text/html'
    switch(extenname)
    {
        case '.js':
            contentType ='text/javascript';
            break;
        case '.css':
            contentType ='text/css';
            break;
        case '.json':
            contentType ='application/json';
            break;
        case '.jpg':
            contentType ='image/jpg';
            break;
        case '.png':
            contentType ='image/png';
            break;
    }
    // Read file
    fs.readFile(filepath, (err, content) =>{
        if(err){
            if(err.code == 'ENOENT') {
            //Page is not found
                fs.readFile(path.join(__dirname, 'public', '404.html'), (err,content)=>
                {
                    res.writeHead(200, {'Content-Type':'text/html'});
                    res.end(content,'utf8');
                })
            }else{
                // Some server error
                res.writeHead(500);
                res.end(`Server Error: ${err.code}`)
            }
        }else{
            // Success
            res.writeHead(200, {'Content-Type': contentType});
            res.end(content,'utf8');
        }
    });
});
 
//console.log(req.url);

const PORT = process.env.PORT || 500;
server.listen(PORT,()=> console.log(`server is running ${PORT}`));